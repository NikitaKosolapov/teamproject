//
//  ViewController.swift
//  Pills
//
//  Created by aprirez on 7/12/21.
//

import UIKit

class DevelopmentViewController: UIViewController {
    
    private var router: DevelopmentRouter!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

}

extension DevelopmentViewController {
    // TODO: remove me when all controllers got ready
    static var bgColors: [UIColor] = [.red, .green, .blue, .black]

    func configure() {
        router = DevelopmentRouter(viewController: self)
        
        // TODO: remove me when all controllers got ready
        view.backgroundColor = DevelopmentViewController.bgColors.first ?? .yellow
        DevelopmentViewController.bgColors.removeFirst()

        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
